﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace MusicDirectory
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SqlConnection sqlConnection;
        public MainWindow()
        {

            InitializeComponent();
            string conn = ConfigurationManager.ConnectionStrings["MusicDirectory.Properties.Settings.MusicStorageDBConnectionString"].ConnectionString;
            sqlConnection = new SqlConnection(conn);
            ShowInterpret();
            
        }
        //Select Interpret
        public void ShowInterpret()
        {
            try
            {
                string query = "select * from Interpret";
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(query, sqlConnection);

                using (sqlDataAdapter)
                {

                    DataTable interpretTable = new DataTable();
                    sqlDataAdapter.Fill(interpretTable);

                    //Welche Informationen der Tabelle in unserem DataTable soll in unsere ComboBox angezeigt werden
                    listInterpret.DisplayMemberPath = "InterpretName";

                    // Welcher Wert soll gegeben werden, wenn eines unserer Items von ComboBox ausgewählt wird
                    listInterpret.SelectedValuePath = "InterpretID";

                    listInterpret.ItemsSource = interpretTable.DefaultView;
                }
            }
            catch (Exception e)
            {

                MessageBox.Show(e.ToString());
            }
            

       
    }
    //Select Titel
    public void ShowAssocietedTitel()
        {

            try
            {
                string query = "select * from Titel t inner join MusicStorage ms on t.TitelID = ms.TitID where ms.InID = @InID";
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);

                using (sqlDataAdapter)
                {

                    sqlCommand.Parameters.AddWithValue("@InID", listInterpret.SelectedValue);

                    DataTable titelTable = new DataTable();
                    sqlDataAdapter.Fill(titelTable);

                    //Welche Informationen der Tabelle in unserem DataTable soll in unsere ComboBox angezeigt werden
                    listTitel.DisplayMemberPath = "TitelName";

                    // Welcher Wert soll gegeben werden, wenn eines unserer Items von ComboBox ausgewählt wird
                    listTitel.SelectedValuePath = "TitelID";

                    listTitel.ItemsSource = titelTable.DefaultView;
                }
            }
            catch (Exception e)
            {

                MessageBox.Show(e.ToString());
            }
            
        }

        private void listInterpret_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ShowAssocietedTitel();
            //MessageBox.Show(listInterpret.SelectedValue.ToString());
        }


    }   
}
